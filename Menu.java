import java.util.Scanner;


public class Menu {
  Scanner input=new Scanner(System.in);
  public void showLoginMenu(){
	  System.out.println();
	  System.out.println("1.登录 2.退出");
	  System.out.println("******************************");
	  System.out.println("请选择：输入数字：");
	  int num=input.nextInt();
	  
	  if(num==1){
		  System.out.println("请输入用户名：");
		  String name=input.next();
		  System.out.println("请输入密码：");
		  String pass=input.next();
		  if(name.equals("admin")&&pass.equals("123")){
			  showMainMenu();
		  }else{
			  System.out.println("您的用户名或密码有误！请重试！");
			  showLoginMenu();
		  }
	  }else if(num==2){
		  System.out.println("谢谢使用！");
	  }else{
		  System.out.println("输入有误！");
		  showLoginMenu();
	  }
  }
  public void showMainMenu(){
	  System.out.println();
	  System.out.println("1客户信息 2真情回馈");
	  System.out.println("*************************");
	  System.out.println("请选择：输入数字或按0返回上一层");
	  int num =input.nextInt();
	  if(num==0){
		  showLoginMenu();
	  }else if(num==1){
		  showCustMenu();
	  }else if(num==2){
		  showSendMenu();
	  }else{
		  System.out.println("输入有误！");
		  showMainMenu();
	  }
  }
  public void showCustMenu(){
	  System.out.println();
	  System.out.println("1查询  2修改 3添加  4显示 ");
	  System.out.println("***********************************");
	  System.out.println("请选择：输入数字或按0返回上一级菜单：");
	  int num=input.nextInt();
	  if(num==0){
		  showMainMenu();
	  }else if(num==1){
		  System.out.println("执行查询");
	  }else if(num==2){
		  System.out.println("执行修改");
	  }else if(num==3){
		  System.out.println("执行添加");
	  }else if(num==4){
		  System.out.println("执行显示");
	  }else{
		  System.out.println("输入错误！");
	  }
	  if(num!=0){
		  showCustMenu();
	  }
  }
  public void showSendMenu(){
	  System.out.println();
	  System.out.println("1幸运大奖  2幸运抽奖 3生日问候");
	  System.out.println("************************");
	  System.out.println("请选择：输入数字或按0返回上一层");
	  int num=input.nextInt();
	  if(num==0){
		  showMainMenu();
	  }else if(num==1){
		  System.out.println("执行幸运大放送");
	  }else if(num==2){
		  System.out.println("执行幸运抽奖");
	  }else if(num==3){
		  System.out.println("执行生日问候");
	  }else{
		  System.out.println("“输入错误！");
	  }
	  if(num!=0){
		  showSendMenu();
	  }
  }
}
