
public class StudentsBiz {
    String[] names=new String[30];
    public void addName(String name){
    	for (int i = 0; i < names.length; i++) {
			if(names[i]==null){
				names[i]= name;
				break;
			}
		}
    }
    public boolean modifyName(String oldName,String newName){
    	int index=searchName(oldName);
    	if(index!=-1){
    		names[index]=newName;
    		return true;
    	}
    	return false;
    }
    public int searchName(String name){
    	for (int i = 0; i < names.length; i++) {
			if(names[i]!=null&&names[i].equals(name)){
				return i;
			}
		}
    	return -1;
    }
    public void showName(){
    	for (int i = 0; i < names.length; i++) {
			if(names[i]!=null){
				System.out.print(names[i]+" ");
			}
		}
    	System.out.println();
    }
    public static void main(String[] args){
    	StudentsBiz sb =new StudentsBiz();
    	sb.addName("张三");
    	sb.addName("李四");
    	sb.addName("王二");
    	sb.addName("麻子");
    	sb.searchName("张");
    	
    }
}
